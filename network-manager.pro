greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
TARGET = network-manager

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR	      = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

# Disable QDebug on Release build
#CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# Unused not working
#CONFIG(release):DEFINES += QT_NO_DEBUG_OUTPUT

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }
        BINDIR = $$PREFIX/bin

        target.path = $$BINDIR

       help.path=$$PREFIX/share/doc/network-manager/help/
       help.files=help/network-manager.html

        desktop.path = $$PREFIX/share/applications/
        desktop.files = "network-manager.desktop"

        icons.path = /usr/share/pixmaps
        icons.files = network-manager.png
        
        SCRIPTS_PATH = $$PREFIX/share/network-manager

        scripts.files = scripts
        scripts.path  = $$SCRIPTS_PATH

        INSTALLS += target  desktop help icons scripts
}

FORMS += \
    mainwindow.ui
HEADERS += \
    mainwindow.h \
    version.h \
    about.h \
    cmd.h
SOURCES += main.cpp \
    mainwindow.cpp \
    about.cpp \
    cmd.cpp
CONFIG += release warn_on thread qt c++11

RESOURCES += \
    images.qrc

